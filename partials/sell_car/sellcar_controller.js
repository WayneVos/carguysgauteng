(function () {
    'use strict';
    var controllerId = 'tradeInController';
    angular.module('app').controller(controllerId, ['$scope', '$http', '$state', 'configService', tradeInFunction]);

    function tradeInFunction($scope, $http, $state, configService) {
        $scope.store = configService;
        var vm = this;
        vm.mmData = [];
        vm.makeData = [];
        vm.modelData = [];
        vm.variantData = [];
        vm.details = {
            make: '',
            model: '',
            year: '',
            mileage: '',
            name: '',
            surname: '',
            number: '',
            email: '',
            price: '',
            colour: '',
            comment: ''
        };
        spinner();

        function spinner() {
            document.getElementById("submit1").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block", "width", "50%");
            });
        }

        var dataUrlBasePath = "https://www.bluechipdealers.co.za/api/v3/view_all_vehicle_codes?";
        ShowController($http, $state);

        function ShowController($http) {
            refreshFilters();
        }

        function getSelectedMakeText() {
            return vm.details.selectedMake.toString() ? vm.details.selectedMake : "";
        }

        function getSelectedSeriesText() {
            return vm.details.selectedSeries.toString() ? vm.details.selectedSeries : "";
        }

        function getSelectedVariantText() {
            return vm.details.selectedVariant.toString() ? vm.details.selectedVariant : "";
        }

        function refreshFilters() {
            $http.get(dataUrlBasePath + "&select=make").then(function (response) {
                vm.makeData = [];
                for (var counter = 0; counter < response.data.length; counter++) {
                    vm.makeData.push(response.data[counter]);
                }
            });
        }

        $scope.selectChangedMake = function () {
            var make = getSelectedMakeText();
            if (make !== "") {
                $http.get(dataUrlBasePath + "&select=model"
                    + (make !== "" ? "&make=eq." + make : "")).then(function (response) {
                    vm.modelData = [];
                    for (var counter = 0; counter < response.data.length; counter++) {
                        vm.modelData.push(response.data[counter]);
                    }
                });
            } else {
            }
        };
        $scope.selectChangedSeries = function () {
            var make = getSelectedMakeText();
            var model = getSelectedSeriesText();
            if (model !== "") {
                $http.get(dataUrlBasePath + "&select=variant,mmcode"
                    + (make !== "" ? "&make=eq." + make : "")
                    + (model !== "" ? "&model=eq." + model : "")).then(function (response) {
                    vm.variantData = [];
                    for (var counter = 0; counter < response.data.length; counter++) {
                        vm.variantData.push(response.data[counter]);
                    }
                });
            } else {
            }
        };
        $scope.selectChangedVariant = function () {
            var make = getSelectedMakeText();
            var model = getSelectedSeriesText();
            var variant = getSelectedVariantText();
            if (getSelectedVariantText() !== "") {
                $http.get(dataUrlBasePath + "&select=mmcode"
                    + (make !== "" ? "&make=eq." + make : "")
                    + (model !== "" ? "&model=eq." + model : "")
                    + (variant !== "" ? "&variant=eq." + variant : "")).then(function (response) {
                    vm.mData = [];
                    for (var counter = 0; counter < response.data.length; counter++) {
                        vm.mData.push(response.data[counter]);
                    }
                    $scope.mCode = vm.mData[0].mmcode;
                });
            } else {
            }
        };
        vm.sendEmail = sendEmail;
        vm.totalFileSize = totalFileSize;

        function buildSimpleMessageWithAttachments(details, files) {
            return {
                ToList: $scope.store.eMail.carSellEmail,//$scope.store.eMail.carSellEmail
                CcList: "",
                BccList: $scope.store.eMail.bccEmail,
                Subject: "I would like to sell my car.",
                Message: buildTradeInMessageString(details),
                MessageSubject: 'I would lke to sell my vehicle',
                ToName: $scope.store.eMail.carSellEmailTo,
                FromName: details.name + ' ' + details.surname,
                FromEmail: details.email,
                Attachments: files
            };
        }

        function checkLeadApi() {
            $scope.url = $state.href($state.current.name, $state.params, {absolute: true});
            var requestError = "";
            var errorData = {
                notification: {
                    ToList: "wayne@vmgsoftware.co.za",
                    CcList: "webmaster@vmgsoftware.co.za",
                    BccList: "",
                    Subject: "Error Lead: " + $scope.store.name,
                    Message: "ERROR:"
                        + "\nUser Agent : " + navigator.userAgent
                        + "\nJWT : " + SUBMIT_LEAD_TOKEN
                        + "\nURL : " + $scope.url,
                    MessageSubject: "Error Lead",
                    ToName: "VMG Admin",
                    FromName: $scope.store.name,
                    FromEmail: ""
                }
            };
            requestError = JSON.stringify(errorData);
            $.ajax({
                url: "https://newsendemail.vmgsoftware.co.za/api/sendemail",
                type: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json"
                },
                data: requestError,
                success: function () {
                },
                error: function () {
                }
            });
        }

        $scope.disabled = false;

        function sendEmail() {
            $scope.disabled = true;
            var sellCarCRM = "";
            var dataCrm = {
                "branch_guid": "", // hardcode branch_guid
                "cellphone_no": vm.details.number.toString(),
                "colour": vm.details.kleur,
                "email_address": vm.details.email,
                "lead_name": vm.details.name + " " + vm.details.surname,
                "lead_source": "Website-Finance",
                "mileage": Number(vm.details.mileage),
                "price": Number(vm.details.price),
                "selling": false,
                "stock_id": Number('0'),
                "year": Number(vm.details.year)
            };
            sellCarCRM = JSON.stringify(dataCrm);
            $.each(vm.flow.files, function (index, file) {
                var progress = file.progress();
                if (progress != 1) {
                    alert("Please wait while loading image " + file.name);
                    return;
                }
            });
            var imageObjs = $('#image-files img');
            var imageContents = {};
            imageObjs.each(function (index, image) {
                imageContents[image.alt] = image.src;
            });
            var notification = buildSimpleMessageWithAttachments(vm.details, imageContents);
            var data = "{'notification':" + JSON.stringify(notification) + "}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "https://newsendemail.vmgsoftware.co.za/api/sendemail",
                data: data,
                success: function (response) {
                    angular.element("#spinner").css("display", "none");
                    document.getElementById("tradeInForm").reset();
                },
                error: function (response) {
                }
            });
            $.ajax({
                url: "/leads/v1/submit/lead",
                type: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json",
                    authorization: "Bearer " + SUBMIT_LEAD_TOKEN
                },
                data: sellCarCRM,
                tryCount: 0,
                retryLimit: 3,
                success: function (response) {
                    console.log('Successful Lead Submit')
                },
                error: function (error) {
                    this.tryCount++;
                    if (this.tryCount <= this.retryLimit) {//try again
                        $.ajax(this);
                        return;
                    } else {
                        checkLeadApi();
                    }
                }
            });
        }

        function totalFileSize(flow) {
            var files = flow.files;
            var sum = 0;
            $.each(files, function (index, file) {
                sum += file.size;
            });
            return sum;
        }

        function buildTradeInMessageString(details) {
            var newLine = '\n';
            return 'Name: ' + details.name + ' ' + details.surname + newLine
                + 'Phone Number: ' + details.number + newLine
                + 'Email: ' + details.email + newLine + newLine
                + 'mmCode: ' + $scope.mCode + newLine
                + 'Make: ' + details.selectedMake + newLine
                + 'Model: ' + details.selectedSeries + newLine
                + 'Variant: ' + details.selectedVariant + newLine
                + 'Year: ' + details.year + newLine
                + 'Mileage: ' + details.mileage + newLine
                + 'Asking Price: ' + details.price + newLine
                + 'Colour: ' + details.colour + newLine
                + 'Comments: ' + details.comment;
        }
    }
})();