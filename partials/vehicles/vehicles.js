(function () {
    "use strict";
    angular.module("app")
        .directive('convertToNumber', function () {
            return {
                require: 'ngModel',
                link: function (scope, element, attrs, ngModel) {
                    ngModel.$parsers.push(function (val) {
                        return val != null ? parseInt(val, 10) : null;
                    });
                    ngModel.$formatters.push(function (val) {
                        return val != null ? '' + val : null;
                    });
                }
            }
        })
        .controller("showroom", ["$scope", "$http", "$state", "$stateParams", function ($scope, $http, $state, $stateParams) {
            var vm = this;
            vm.data = [];
            vm.selectData = [];
            vm.detailsdata = {};
            vm.makeData = [];
            vm.seriesData = [];
            vm.colourData = [];
            vm.yearData = [];
            vm.bodyData = [];
            $scope.selectMake = [];
            $scope.selectSeries = [];
            $scope.selectColour = [];
            $scope.selectYear = [];
            $scope.selectBody = [];
            $scope.selectMinPrice = {};
            $scope.selectMaxPrice = [];
            $scope.selectMinMile = [];
            $scope.selectMaxMile = [];
            $scope.limitSet = 12;
            $scope.perPage = 12;
            var companyIdFilter = "company_id=eq.413";
            var dataUrl = "https://carguyssa.co.za/service_api/view_stock_complete?" + companyIdFilter;

            function getSelectedMake() {
                return $scope.selectMake ? $scope.selectMake.make : "";
            }

            function getSelectedSeries() {
                return $scope.selectSeries ? $scope.selectSeries.series : "";
            }

            function getSelectedMinPrice() {
                return $scope.selectMinPrice ? $scope.selectMinPrice : "";
            }

            function getSelectedMaxPrice() {
                return $scope.selectMaxPrice ? $scope.selectMaxPrice : "";
            }

            function getSelectedMinMile() {
                return $scope.selectMinMile ? $scope.selectMinMile : "";
            }

            function getSelectedMaxMile() {
                return $scope.selectMaxMile ? $scope.selectMaxMile : "";
            }

            function getSelectedYear() {
                return $scope.selectYear ? $scope.selectYear.year : "";
            }

            function getSelectedColour() {
                return $scope.selectColour ? $scope.selectColour.colour : "";
            }

            function getSelectedBody() {
                return $scope.selectBody ? $scope.selectBody.body_type : "";
            }

            function fetchData() {
                var filterState = (getSelectedMake() !== "" ? "&make=eq." + getSelectedMake() : "") +
                    (getSelectedSeries() !== "" ? "&series=eq." + getSelectedSeries() : "") +
                    (getSelectedYear() !== "" ? "&year=eq." + getSelectedYear() : "") +
                    (getSelectedColour() !== "" ? "&colour=eq." + getSelectedColour() : "") +
                    (getSelectedBody() !== "" ? "&body_type=eq." + getSelectedBody() : "") +
                    (getSelectedMinPrice() !== "" ? "&selling_price=gte." + getSelectedMinPrice() : "") +
                    (getSelectedMaxPrice() !== "" ? "&selling_price=lte." + getSelectedMaxPrice() : "") +
                    (getSelectedMinMile() !== "" ? "&mileage=gte." + getSelectedMinMile() : "") +
                    (getSelectedMaxMile() !== "" ? "&mileage=lte." + getSelectedMaxMile() : "");
                $http.get(dataUrl + filterState).then(function (response) {
                    vm.data = response.data;
                    $scope.totalCars = vm.data.length;
                })
            }

            function initLoad() {
                $scope.selectMake = $stateParams.make;
                $scope.selectSeries = $stateParams.series;
                $scope.selectYear = $stateParams.year;
                $scope.selectColour = $stateParams.colour;
                $scope.selectBody = $stateParams.body_type;
                $scope.selectMinPrice = $stateParams.minP;
                $scope.selectMaxPrice = $stateParams.maxP;
                $scope.selectMinMile = $stateParams.minM;
                $scope.selectMaxMile = $stateParams.maxM;
                $http.get(dataUrl + "&limit=" + $scope.limitSet + selectFilter()).then(function (response) {
                    vm.data = response.data;
                    for (var d = 0; d < response.data.length; d++) {
                        response.data[d].url1 = response.data[d].url1.replace(/^http:\/\//i, 'https://');
                    }
                });
            }

            function loadData() {
                $http.get(dataUrl + "&select=make,series,year,colour,body_type,year" + selectFilter()).then(function (response) {
                    vm.makeData = [];
                    vm.seriesData = [];
                    vm.colourData = [];
                    vm.yearData = [];
                    vm.bodyData = [];
                    for (var i = 0; i < response.data.length; i++) {
                        vm.makeData.push(response.data[i]);
                        vm.seriesData.push(response.data[i]);
                        vm.colourData.push(response.data[i]);
                        vm.yearData.push(response.data[i]);
                        vm.bodyData.push(response.data[i]);
                    }
                });
            }

            function selectFilter() {
                var filterUrl = '';
                if ($stateParams.make !== null && $stateParams.make !== undefined) {
                    filterUrl += "&make=eq." + $stateParams.make;
                    var make = {make: $state.params.make};
                    vm.makeData.push(make);
                    $scope.selectMake = vm.makeData[0];
                    $scope.limitSet = "All";
                }
                if ($stateParams.series !== null && $stateParams.series !== undefined) {
                    filterUrl += "&series=eq." + $stateParams.series;
                    var series = {series: $state.params.series};
                    vm.seriesData.push(series);
                    $scope.selectSeries = vm.seriesData[0];
                    $scope.limitSet = "All";
                }
                if ($stateParams.year !== null && $stateParams.year !== undefined) {
                    filterUrl += "&year=eq." + $stateParams.year;
                    var year = {year: $state.params.year};
                    vm.yearData.push(year);
                    $scope.selectYear = vm.yearData[0];
                    $scope.limitSet = "All";
                }
                if ($stateParams.colour !== null && $stateParams.colour !== undefined) {
                    filterUrl += "&colour=eq." + $stateParams.colour;
                    var colour = {colour: $state.params.colour};
                    vm.colourData.push(colour);
                    $scope.selectColour = vm.colourData[0];
                    $scope.limitSet = "All";
                }
                if ($stateParams.body_type !== null && $stateParams.body_type !== undefined) {
                    filterUrl += "&body_type=eq." + $stateParams.body_type;
                    var body = {body_type: $state.params.body_type};
                    vm.bodyData.push(body);
                    $scope.selectBody = vm.bodyData[0];
                    $scope.limitSet = "All";
                }
                if ($stateParams.minP !== null && $stateParams.minP !== undefined) {
                    filterUrl += "&selling_price=gte." + $stateParams.minP;
                    $scope.selectMinPrice = $stateParams.minP;
                    $scope.limitSet = "All";
                }
                if ($stateParams.maxP !== null && $stateParams.maxP !== undefined) {
                    filterUrl += "&selling_price=lte." + $stateParams.maxP;
                    $scope.selectMaxPrice = $stateParams.maxP;
                    $scope.limitSet = "All";
                }
                if ($stateParams.minM !== null && $stateParams.minM !== undefined) {
                    filterUrl += "&mileage=gte." + $stateParams.minM;
                    $scope.selectMinMile = $stateParams.minM;
                    $scope.limitSet = "All";
                }
                if ($stateParams.maxM !== null && $stateParams.maxM !== undefined) {
                    filterUrl += "&mileage=lte." + $stateParams.maxM;
                    $scope.selectMaxMile = $stateParams.maxM;
                    $scope.limitSet = "All";
                }
                return filterUrl;
            }

            showRoom($http, $state);
            showRoom.$inject = [$http, $state, $stateParams];

            function showRoom() {
                loadData();
                initLoad();
                vm.detailsClick = function (car) {
                    vm.detailsdata = car;
                    $stateParams.car = car;
                    $state.go("cardetails", {
                        stock_id: car.stock_id,
                        series: car.series,
                        make: car.make,
                        year: car.year,
                        selling_price: car.selling_price
                    }, {});
                };
            }

            $scope.changedMake = function () {
                var make = getSelectedMake();
                if (make !== "") {
                    $http.get(dataUrl + "&select=series,year,colour,body_type,year"
                        + (make !== "" ? "&make=eq." + make : "")).then(function (response) {
                        vm.seriesData = [];
                        vm.colourData = [];
                        vm.yearData = [];
                        vm.bodyData = [];
                        for (var i = 0; i < response.data.length; i++) {
                            vm.seriesData.push(response.data[i]);
                            vm.colourData.push(response.data[i]);
                            vm.yearData.push(response.data[i]);
                            vm.bodyData.push(response.data[i]);
                        }
                    });
                } else {
                    initLoad()
                }
                stateSet();
                fetchData();
            };
            $scope.changedSeries = function () {
                var make = getSelectedMake();
                var series = getSelectedSeries();
                if (series !== "") {
                    $http.get(dataUrl + "&select=year,colour,body_type,year"
                        + (make !== "" ? "&make=eq." + make : "")
                        + (series !== "" ? "&series=eq." + series : "")).then(function (response) {
                        vm.colourData = [];
                        vm.yearData = [];
                        vm.bodyData = [];
                        for (var i = 0; i < response.data.length; i++) {
                            vm.colourData.push(response.data[i]);
                            vm.yearData.push(response.data[i]);
                            vm.bodyData.push(response.data[i]);
                        }
                    });
                } else {
                    initLoad()
                }
                stateSet();
                fetchData();
            };
            $scope.changedYear = function () {
                var make = getSelectedSeries();
                var series = getSelectedSeries();
                var year = getSelectedYear();
                if (year !== "") {
                    $http.get(dataUrl + "&select=colour,body_type,year"
                        + (make !== "" ? "&make=eq." + make : "")
                        + (series !== "" ? "&series=eq." + series : "")
                        + (year !== "" ? "&year=eq." + year : "")).then(function (response) {
                        vm.colourData = [];
                        vm.bodyData = [];
                        for (var i = 0; i < response.data.length; i++) {
                            vm.colourData.push(response.data[i]);
                            vm.bodyData.push(response.data[i]);
                        }
                    });
                }
                stateSet();
                fetchData();
            };
            $scope.changedColour = function () {
                var make = getSelectedSeries();
                var series = getSelectedSeries();
                var year = getSelectedYear();
                var colour = getSelectedColour();
                if (colour !== "") {
                    $http.get(dataUrl + "&select=body_type,year"
                        + (make !== "" ? "&make=eq." + make : "")
                        + (series !== "" ? "&series=eq." + series : "")
                        + (year !== "" ? "&year=eq." + year : "")
                        + (colour !== "" ? "&colour=eq." + colour : "")).then(function (response) {
                        vm.bodyData = [];
                        for (var i = 0; i < response.data.length; i++) {
                            vm.bodyData.push(response.data[i]);
                        }
                    });
                }
                stateSet();
                fetchData();
            };
            $scope.changedBody = function () {
                if (getSelectedBody() !== "") {
                }
                stateSet();
                fetchData();
            };
            $scope.changedMinPrice = function () {
                var minPrice = getSelectedMinPrice();
                if (minPrice !== "") {
                }
                stateSet();
                fetchData();
            };
            $scope.changedMaxPrice = function () {
                var maxPrice = getSelectedMaxPrice();
                if (maxPrice !== "") {
                }
                stateSet();
                fetchData();
            };
            $scope.changedMinMile = function () {
                var minMile = getSelectedMinMile();
                if (minMile !== "") {
                }
                stateSet();
                fetchData();
            };
            $scope.changedMaxMile = function () {
                var maxMile = getSelectedMaxMile();
                if (maxMile !== "") {
                }
                stateSet();
                fetchData();
            };

            function stateSet() {
                var minP = $scope.selectedMinPrice;
                var maxP = $scope.selectedMaxPrice;
                var maxpTrimmed = parseInt(maxP);
                var minpTrimmed = parseInt(minP);
                if (!isNaN(minpTrimmed) && minpTrimmed !== null && minpTrimmed !== undefined) {
                    $stateParams.minPrice = minP;
                    if (maxpTrimmed < minpTrimmed) {
                        $scope.selectedMinPrice = "0";
                        $stateParams.minPrice = $scope.selectedMinPrice;
                    }
                }
                if (!isNaN(maxpTrimmed) && maxpTrimmed !== null && maxpTrimmed !== undefined) {
                    $stateParams.maxPrice = maxP;
                }
                $stateParams.make = getSelectedMake();
                $stateParams.series = getSelectedSeries();
                $stateParams.minP = getSelectedMinPrice();
                $stateParams.maxP = getSelectedMaxPrice();
                $stateParams.minM = getSelectedMinMile();
                $stateParams.maxM = getSelectedMaxMile();
                $stateParams.year = getSelectedYear();
                $stateParams.colour = getSelectedColour();
                $stateParams.body_type = getSelectedBody();
                $state.go('.', {
                    make: $stateParams.make,
                    series: $stateParams.series,
                    minP: $stateParams.minP,
                    maxP: $stateParams.maxP,
                    minM: $stateParams.minM,
                    maxM: $stateParams.maxM,
                    year: $stateParams.year,
                    colour: $stateParams.colour,
                    body_type: $stateParams.body_type
                }, {notify: false});
            }

            $scope.clearFilter = function () {
                initLoad();
                $stateParams.make = "";
                $stateParams.series = "";
                $stateParams.minP = "";
                $stateParams.maxP = "";
                $stateParams.minM = "";
                $stateParams.maxM = "";
                $stateParams.year = "";
                $stateParams.colour = "";
                $stateParams.body_type = "";
                $state.go('.', {
                    make: $stateParams.make,
                    series: $stateParams.series,
                    minP: $stateParams.minP,
                    maxP: $stateParams.maxP,
                    minM: $stateParams.minM,
                    maxM: $stateParams.maxM,
                    year: $stateParams.year,
                    colour: $stateParams.colour,
                    body_type: $stateParams.body_type
                }, {});
            };
            $scope.back = function () {
                window.history.back();
            };
            $scope.scrollToTop = function () {
                if (typeof jQuery == 'undefined') {
                    return window.scrollTo(0, 0);
                } else {
                    var body = $('html, body');
                    body.animate({scrollTop: 0}, '600', 'swing');
                }
                return true;
            };
        }
        ])
})();