(function () {
    'use strict';
    var controllerId = 'showroomFinance';
    angular.module('app').controller(controllerId, ['$scope', '$http', '$state', 'configService', finApp]);

    function finApp($scope, $http, $state, configService) {
        $scope.store = configService;
        var vm = this;
        vm.details = {
            name: "",
            lastname: "",
            idnumber: "",
            number: "",
            email: "",
            message: "",
            permission: "",
            deposit: "",
            depositprice: "",
            tradein: "",
            tradeindetails: ""
        };
        spinner();

        function spinner() {
            document.getElementById("submit1").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block", "width", "50%");
            });
        }

        vm.sendEmail = sendEmail;
        vm.totalFileSize = totalFileSize;
        $scope.imageStrings = [];

        function processFiles(files) {
            angular.forEach(files, function (flowFile, i) {
                var fileReader = new FileReader();
                fileReader.onload = function (event) {
                    var uri = event.target.result;
                    $scope.imageStrings[i] = uri;
                };
                fileReader.readAsDataURL(flowFile.file);
            });
        }

        $scope.processFiles = processFiles;

        function buildSimpleMessageWithAttachments(details, files) {
            return {
                ToList: $scope.store.eMail.carSellEmail,//$scope.store.eMail.carSellEmail
                CcList: "",
                BccList: $scope.store.eMail.bccEmail,
                Subject: "I would like to Apply for finance.",
                Message: buildTradeInMessageString(details),
                MessageSubject: 'I would like to Apply for finance.',
                ToName: $scope.store.eMail.carSellEmailTo,
                FromName: details.name + ' ' + details.lastname,
                FromEmail: details.email,
                Attachments: files
            };
        }

        function checkLeadApi() {
            $scope.url = $state.href($state.current.name, $state.params, {absolute: true});
            var driveRequestError = "";
            var errorData = {
                notification: {
                    ToList: "wayne@vmgsoftware.co.za",
                    CcList: "webmaster@vmgsoftware.co.za",
                    BccList: "",
                    Subject: "Error Lead: " + $scope.store.name,
                    Message: "ERROR:"
                        + "\nUser Agent : " + navigator.userAgent
                        + "\nJWT : " + SUBMIT_LEAD_TOKEN
                        + "\nURL : " + $scope.url,
                    MessageSubject: "Error Lead",
                    ToName: "VMG Admin",
                    FromName: $scope.store.name,
                    FromEmail: ""
                }
            };
            driveRequestError = JSON.stringify(errorData);
            $.ajax({
                url: "https://newsendemail.vmgsoftware.co.za/api/sendemail",
                type: "POST",
                 headers: {
                    "content-type": "application/json",
                    accept: "application/json"
                },
                data: driveRequestError,
                success: function () {
                },
                error: function () {
                }
            });
        }

        $scope.disabled = false;

        function sendEmail() {
            $scope.disabled = true;
            var financeRequest = "";
            var dataCrm = {
                "branch_guid": "", // hardcode branch_guid
                "cellphone_no": vm.details.number.toString(),
                "colour": "None",
                "email_address": vm.details.email,
                "lead_name": vm.details.name + " " + vm.details.lastname,
                "lead_source": "Website-Finance",
                "mileage": Number('0'),
                "price": Number('0'),
                "selling": false,
                "stock_id": Number('0'),
                "year": Number('0')
            };
            financeRequest = JSON.stringify(dataCrm);
            var imageContents = {};
            var notification = buildSimpleMessageWithAttachments(vm.details, imageContents);
            var data = "{'notification':" + JSON.stringify(notification) + "}";
            var maxJsonLength = 100485760; // 10 MiB
            if (data.length > maxJsonLength) {
                alert("Your files are too big (more than 10 MiB). Please reduce the number and/or size of the files");
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "https://newsendemail.vmgsoftware.co.za/api/sendemailWithAttachments",
                data: data,
                success: function (response) {
                    angular.element("#spinner").css("display", "none");
                    /* var delayredirect = 500;
                     setTimeout(function () {
                     window.location = '.';
                     }, delayredirect);*/
                },
                error: function (response) {
                }
            });
            $.ajax({
                url: "/leads/v1/submit/lead",
                type: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json",
                    authorization: "Bearer " + SUBMIT_LEAD_TOKEN
                },
                data: financeRequest,
                tryCount: 0,
                retryLimit: 3,
                success: function (response) {
                    console.log('Successful Lead Submit')
                },
                error: function (error) {
                    this.tryCount++;
                    if (this.tryCount <= this.retryLimit) { //try again
                        $.ajax(this);
                        return;
                    } else {
                        // checkLeadApi();
                    }
                }
            });
        }

        function totalFileSize(flow) {
            var files = flow.files;
            var sum = 0;
            $.each(files, function (index, file) {
                sum += file.size;
            });
            return sum;
        }

        function buildTradeInMessageString(details) {
            console.log("get details");
            var newLine = '\n';
            return 'Name: ' + details.name + ' ' + details.lastname + newLine
                + "Identity or Passport Number: " + details.idnumber + newLine
                + "Contact Number: " + details.number + newLine
                + "Email: " + details.email + newLine
                + "Message: " + details.message + newLine
                + "NCR Credit Check: " + details.permission + newLine
                + "Deposit: " + details.deposit + newLine
                + "Deposit Amount: " + details.depositprice + newLine
                + "Trade In: " + details.tradein + newLine
                + "Trade In Details: " + details.tradeindetails;
        }
    }
})();