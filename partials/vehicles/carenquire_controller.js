(function () {
    var controllerId = "carEnquire";
    angular.module("app")
        .controller(controllerId, ['$scope', 'configService', '$state', testDrive]);

    function testDrive($scope, configService, $state) {
        $scope.store = configService;
        var vm = this;
        vm.drive = {
            name: "",
            number: "",
            email: "",
            message: "",
            make: "",
            variant: "",
            series: "",
            year: "",
            selling_price: "",
            dealer_email: "",
            dealer_name: "",
            company_id: "",
            guid: "",
            colour: "",
            lead_source: "",
            mileage: "",
            price: "",
            selling: "",
            stock_id: ""
        };

        function spinner() {
            document.getElementById("submitbut2").addEventListener("click", function () {
                angular.element("#spinner").show();
            });
        }

        spinner();
        $scope.disabled = false;
        vm.sendMail = sendEmail;

        function sendEmail() {
            $scope.disabled = true;
            var driveRequest = "";
            var driveRequestCRM = "";
            var data = {
                notification: {
                    ToList: $scope.store.eMail.carEnquireEmail,//$scope.store.eMail.carEnquireEmail
                    CcList: vm.drive.email,// vm.drive.email
                    BccList: $scope.store.eMail.bccEmail,
                    Subject: "Car Guys Enquiry from " + vm.drive.name + ", with regards to: " + vm.drive.make,
                    Message: vm.drive.message
                        + "\nContact Number: " + vm.drive.number
                        + "\nStock ID: " + vm.drive.stock_id
                        + "\nMake: " + vm.drive.make
                        + "\nVariant: " + vm.drive.variant
                        + "\nYear: " + vm.drive.year
                        + "\nPrice: R" + vm.drive.selling_price,
                    MessageSubject: "Car Guys Enquiry from " + vm.drive.name,
                    ToName: $scope.store.eMail.carEnquireEmailTo,
                    FromName: vm.drive.name,
                    FromEmail: vm.drive.email,
                    Attachments: {}
                }
            };
            var dataCrm = {
                "branch_guid": "79b292ed-6385-411f-a84a-1f13908d8b59",
                "cellphone_no": vm.drive.number.toString(),
                "colour": vm.drive.colour,
                "email_address": vm.drive.email,
                "lead_name": vm.drive.name,
                "lead_source": "Car Guys Website",
                "make": vm.drive.make,
                "model": vm.drive.series,
                "mileage": Number(vm.drive.mileage),
                "price": Number(vm.drive.selling_price),
                "selling": false,
                "stock_id": Number(vm.drive.stock_id),
                "message": vm.drive.message,
                "year": Number(vm.drive.year)
            };
            driveRequest = JSON.stringify(data);
            driveRequestCRM = JSON.stringify(dataCrm);
            $.ajax({
                url: "https://newsendemail.vmgsoftware.co.za/api/sendemail",
                type: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json"
                },
                data: driveRequest,
                success: function (response) {
                    document.getElementById("carenquire").reset();
                    angular.element("#carenquire").hide();
                    angular.element("#mailsent").show();
                    angular.element("#spinner").hide();
                },
                error: function (response) {
                    document.getElementById("carenquire").reset();
                    angular.element("#carenquire").hide();
                    angular.element("#mailnotsent").show();
                }
            });
            $.ajax({
                url: "/leads/v1/submit/lead",
                type: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json",
                    authorization: "Bearer " + SUBMIT_LEAD_TOKEN
                },
                data: driveRequestCRM,
                success: function (response) {
                },
                error: function (response) {
                }
            });
        };
    }
})();