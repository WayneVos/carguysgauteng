(function () {
    "use strict";
    angular.module("app")
       .controller("homebloks", function () {
           var bl1Img = document.getElementById("bl1Img");
           var bl1 = document.getElementById("homeblok_one");
           var bl2Img = document.getElementById("bl2Img");
           var bl2 = document.getElementById("homeblok_two");
           var bl3Img = document.getElementById("bl3Img");
           var bl3 = document.getElementById("homeblok_three");
           var bl4Img = document.getElementById("bl4Img");
           var bl4 = document.getElementById("homeblok_four");
           var gr2_bl1Img = document.getElementById("gr2_bl1Img");
           var gr2_bl1 = document.getElementById("gr2_homeblok_one");
           var gr2_bl2Img = document.getElementById("gr2_bl2Img");
           var gr2_bl2 = document.getElementById("gr2_homeblok_two");
           var gr2_bl3Img = document.getElementById("gr2_bl3Img");
           var gr2_bl3 = document.getElementById("gr2_homeblok_three");
           var gr2_bl4Img = document.getElementById("gr2_bl4Img");
           var gr2_bl4 = document.getElementById("gr2_homeblok_four");
           bl1Img.addEventListener("mouseover", function () {
               bl1.classList.remove("slideOutDown");
               bl1.classList.add("slideInUp");
           });
           bl1.addEventListener("mouseout", function () {
               bl1.classList.remove("slideInUp");
               bl1.classList.add("slideOutDown");
           });
           bl2Img.addEventListener("mouseover", function () {
               bl2.classList.remove("slideOutDown");
               bl2.classList.add("slideInUp");
           });
           bl2.addEventListener("mouseout", function () {
               bl2.classList.remove("slideInUp");
               bl2.classList.add("slideOutDown");
           });
           bl3Img.addEventListener("mouseover", function () {
               bl3.classList.remove("slideOutDown");
               bl3.classList.add("slideInUp");
           });
           bl3.addEventListener("mouseout", function () {
               bl3.classList.remove("slideInUp");
               bl3.classList.add("slideOutDown");
           });
           bl4Img.addEventListener("mouseover", function () {
               bl4.classList.remove("slideOutDown");
               bl4.classList.add("slideInUp");
           });
           bl4.addEventListener("mouseout", function () {
               bl4.classList.remove("slideInUp");
               bl4.classList.add("slideOutDown");
           });
           gr2_bl1Img.addEventListener("mouseover", function () {
               gr2_bl1.classList.remove("slideOutDown");
               gr2_bl1.classList.add("slideInUp");
           });
           gr2_bl1.addEventListener("mouseout", function () {
               gr2_bl1.classList.remove("slideInUp");
               gr2_bl1.classList.add("slideOutDown");
           });
           gr2_bl2Img.addEventListener("mouseover", function () {
               gr2_bl2.classList.remove("slideOutDown");
               gr2_bl2.classList.add("slideInUp");
           });
           gr2_bl2.addEventListener("mouseout", function () {
               gr2_bl2.classList.remove("slideInUp");
               gr2_bl2.classList.add("slideOutDown");
           });
           gr2_bl3Img.addEventListener("mouseover", function () {
               gr2_bl3.classList.remove("slideOutDown");
               gr2_bl3.classList.add("slideInUp");
           });
           gr2_bl3.addEventListener("mouseout", function () {
               gr2_bl3.classList.remove("slideInUp");
               gr2_bl3.classList.add("slideOutDown");
           });
           gr2_bl4Img.addEventListener("mouseover", function () {
               gr2_bl4.classList.remove("slideOutDown");
               gr2_bl4.classList.add("slideInUp");
           });
           gr2_bl4.addEventListener("mouseout", function () {
               gr2_bl4.classList.remove("slideInUp");
               gr2_bl4.classList.add("slideOutDown");
           });
       })
})();