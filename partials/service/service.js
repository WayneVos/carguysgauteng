(function () {
    var controllerId = "bookService";
    angular.module("app").controller(controllerId, ['$scope', 'configService', bookService]);

    function bookService($scope, configService) {
        $scope.store = configService;
        var vm = this;
        vm.service = {
            early_date: "",
            late_date: "",
            regNo: "",
            make: "",
            model: "",
            vin: "",
            firstReg: "",
            km: "",
            company: "",
            title: "",
            fName: "",
            lName: "",
            mobile: "",
            landline: "",
            email: "",
            isCustomer: "",
            serviceType: "",
            payMethod: ""
        };

        function spinner() {
            document.getElementById("submit").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block");
            });
        }

        spinner();
        vm.sendMail = sendEmail;
        $scope.disabled = false;

        function sendEmail() {
            $scope.disabled = true;
            var serviceRequest = "";
            var data = {
                notification: {
                    ToList: $scope.store.eMail.carServiceEmail,//$scope.store.eMail.carServiceEmail
                    CcList: "",
                    BccList: "webmaster@vmgsoftware.co.za",
                    Subject: "Service reguest from " + vm.service.name,
                    Message: "I would like to book a service for:"
                    + "\n\nEarliest Date: " + vm.service.early_date
                    + "\nLatest Date: " + vm.service.late_date
                    + "\nRegistration Number: " + vm.service.regNo
                    + "\nMake: " + vm.service.make
                    + "\nModel: " + vm.service.model
                    + "\nVin Number (Placed in Windscreen Corner): " + vm.service.vin
                    + "\nYear of 1st registration: " + vm.service.firstReg
                    + "\nKilometres: " + vm.service.km
                    + "\nCompany: " + vm.service.company
                    + "\nTitle: " + vm.service.title
                    + "\nFirst Name: " + vm.service.fName
                    + "\nSurname: " + vm.service.lName
                    + "\nCellphone: " + vm.service.mobile
                    + "\nPhone: " + vm.service.landline
                    + "\nEmail: " + vm.service.email
                    + "\nI am an existing customer: " + vm.service.isCustomer
                    + "\nService Type: " + vm.service.serviceType
                    + "\nPayment method: " + vm.service.payMethod,
                    MessageSubject: "Service reguest from " + vm.service.name,
                    ToName: $scope.store.eMail.carServiceEmailTo,
                    FromName: vm.service.name,
                    FromEmail: vm.service.email,
                    Attachments: {}
                }
            };
            serviceRequest = JSON.stringify(data);
            $.ajax({
                url: "https://newsendemail.vmgsoftware.co.za/api/sendemail",
                type: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json"
                },
                data: serviceRequest,

                success: function (response) {
                    document.getElementById("serviceForm").reset();
                    angular.element("#serviceForm").css("display", "none");
                    angular.element("#mailsent").css("display", "block");
                    angular.element("#spinner").css("display", "none");
                },
                error: function (response) {
                    document.getElementById("serviceForm").reset();
                    angular.element("#serviceForm").css("display", "none");
                    angular.element("#mailnotsent").css("display", "block");
                }
            });
        };
    }
})();