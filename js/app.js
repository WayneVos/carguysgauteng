(function () {
    "use strict";
    angular.module('app', [
           "ngResource",
           "angular-flexslider",
           "angularValidator",
           "ngSanitize",
           "ui.router",
           "updateMeta",
           "flow",
           "angularUtils.directives.dirPagination",
           "angular.filter",
           "angularLazyLoad",
           "angular-img-lazy-load",
           "ngParallax",
           "ngAnimate",
           "ng-currency"
       ])
       .controller("main_ctr", [function () {
       }
       ]);
    $(document).on("click", ".navbar-collapse.in", function (e) {
        if ($(e.target).is("a")) {
            $(this).collapse("hide");
        }
    });
    $('ul.nav li.dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
})();