(function () {
    var app = angular.module("app");
    app.factory('configService', function () {
        var store = {
            name: "Car Guys",
            branch1: "Car Guys",
            email1: "admin@carguys.co.za",
            facebookUrl: "",
            instaUrl: " ",
            twitterUrl: "",
            googleplusUrl: " ",
            youtubeUrl: "",
            address1: "603 Nico Smith Street, Gezina, Pretoria 0185",
            tel1: "012 403 0779‬",
            tel2: "",
            cell1: "082 703 8316",
            fax1: "",
            eMail: {
                contactEmail: "customercare@carguys.co.za",
                contactEmailTo: "Car Guys",
                carRequestEmail: "info@carguyssa.co.za",
                carRequestEmailTo: "Car Guys",
                carSellEmail: "info@carguyssa.co.za",
                carSellEmailTo: "Car Guys",
                carServiceEmail: "customercare@carguys.co.za",
                carServiceEmailTo: "Car Guys",
                carFinanceEmail: "finance@carguys.co.za",
                carFinanceEmailTo: "Car Guys",
                carEnquireEmail: "info@carguyssa.co.za",
                carEnquireEmailTo: "Car Guys",
                testimonialEmail: "info@carguyssa.co.za",
                testimonialEmailTo: "Car Guys",
                bccEmail: "webmaster@vmgsoftware.co.za",
                ccEmail: ""
            },
            tradinghours: {
                monThur: '08:00-17:30',
                fri: '08:00-17:00',
                sat: '09:00 – 12:00',
                sun: 'Closed',
                pub: 'Closed'
            },
            latlong: '-33.9017215,18.6155452',
            latlong1: '',
            mapLink: "https://maps.google.com/maps?ll=-26.865204,26.645428&z=16&t=m&hl=en-GB&gl=ZA&mapclient=embed&cid=17046140541030548605",
            mapLinkbranch1: "https://maps.google.com/maps?ll=-26.865204,26.645428&z=16&t=m&hl=en-GB&gl=ZA&mapclient=embed&cid=17046140541030548605",
            mapLinkShortLink1: ""
        };
        return store;
    });
})();