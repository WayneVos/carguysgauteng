(function () {
    angular.module("app")
       .controller("slider", ["$scope", function ($scope) {
           $scope.slides = [
               {
                   url: "img/slides/s1.jpg",
                   text1: "GRAND i10",
                   text2: "Its Wowsome!",
                   text4: "Bold new looks • New levels of connectivity • Unbeatable space and practicality"
               },
               {
                   url: "img/slides/s3.jpg",
                   text1: "TUCSON",
                   text2: "BORN DYNAMIC",
                   text4: "FUSION OF INNOVATION & PREMIUM COMFORT"
               },
               {
                   url: "img/slides/s2.jpg",
                   text1: "GRAND i10",
                   text3: "EXPERIENCE DYNAMIC SPACE IN COMPACT DIMENSION",
                   text4: "Generous dimension of GRAND i10 boasts its dynamic comfort with class-leading roominess and smart technology features that elevate the confidence and pleasure of your driving experience."
               }
           ];
           $scope.sliderOptions = {
               controlNav: false,
               animationLoop: true,
           }
       }
       ])
})();