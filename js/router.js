(function () {
    var app = angular.module("app");
    app.run(function ($rootScope, $state, $stateParams, $anchorScroll) {
        $rootScope.$on('$stateChangeStart', function () {
            $anchorScroll();
        });
    });
    app.config(function ($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider, $locationProvider) {
        var vehicleType = {
            encode: function (str) {
                return str && str.replace(/ /g, "-");
            },
            decode: function (str) {
                return str && str.replace(/-/g, " ");
            },
            is: angular.isString,
            pattern: /[^/]+/
        };
        $urlMatcherFactoryProvider.type('vehicle', vehicleType);
        $stateProvider
           .state("/", {
               url: "/",
               views: {
                   "": {templateUrl: "partials/home/home.html"},
                   "header@": {templateUrl: "templates/header.html"},
                   "footer@": {templateUrl: "templates/footer.html"}

               },
               controller: "PageCtrl"
           })
           .state("home", {
               url: "/home",
               views: {
                   "": {templateUrl: "partials/home/home.html"},
                   "header@": {templateUrl: "templates/header.html"},
                   "footer@": {templateUrl: "templates/footer.html"},
               },
               controller: "PageCtrl"
           })
           .state("finance", {
               url: "/finance",
               views: {
                   "": {templateUrl: "partials/finance/finance.html"},
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"}
               },
               controller: "PageCtrl"
           })
           .state("sell", {
               url: "/sell",
               views: {
                   "": {
                       templateUrl: "partials/sell_car/sell_car.html",
                       controller: "PageCtrl"
                   },
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"}
               }
           })
           .state("bookservice", {
               url: "/book-service",
               views: {
                   "": {templateUrl: "partials/service/book_service.html"},
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"}
               },
               controller: "PageCtrl"
           })
           .state("vehicles", {
               url: "/showroom?make&series&minP&maxP&minM&maxM&year&colour&body_type",
               param: {
                   car: null,
                   make: null,
                   series: null,
                   minP: null,
                   maxP: null,
                   minM: null,
                   maxM: null,
                   year: null,
                   colour: null,
                   body_type: null
               },
               resolve: {
                   car: function ($stateParams) {
                       return {
                           car: $stateParams.car,
                           make: $stateParams.make,
                           year: $stateParams.year,
                           series: $stateParams.series,
                           minP: $stateParams.minP,
                           maxP: $stateParams.maxP,
                           minM: $stateParams.minM,
                           maxM: $stateParams.maxM,
                           body_type: $stateParams.body_type,
                           colour: $stateParams.colour
                       }
                   }
               },
               views: {
                   "": {templateUrl: "partials/vehicles/vehicles.html"},
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"},
               },
               controller: "PageCtrl"
           })
           .state("cardetails", {
               url: "/used-cars/:stock_id/:make/:series/:year/:selling_price",
               params: {
                   car: null,
                   stock_id: null,
                   year: null,
                   make: null,
                   series: null,
                   selling_price: null
               },
               resolve: {
                   car: function ($stateParams) {
                       return {
                           car: $stateParams.car,
                           stock_id: $stateParams.stock_id,
                           make: $stateParams.make,
                           year: $stateParams.year,
                           series: $stateParams.series,
                           selling_price: $stateParams.selling_price
                       }
                   }
               },
               views: {
                   "": {
                       templateUrl: "partials/vehicles/cardetails.html",
                       controller: "Details"
                   },
                   "header@": {templateUrl: "templates/header.html"},
                   "footer@": {templateUrl: "templates/footer.html"}
               }
           })
           .state("404", {
               url: "/404",
               templateUrl: "partials/404.html",
               controller: "PageCtrl"
           })
           .state("otherwise", {
               url: "/404",
               templateUrl: "partials/404.html",
               controller: "PageCtrl"
           });
        $urlRouterProvider.when("", "/home");
        $urlRouterProvider.otherwise(function ($injector, $location) {
            $injector.invoke(['$state', function ($state) {
                $state.go('404');
            }
            ]);
            return true;
        });
        $locationProvider.html5Mode(true);
    });
    app.controller("PageCtrl", function () {
    });
    var dataUrlBasePath = "https://carguyssa.co.za/service_api/view_stock_complete?company_id=eq.413&select=*";
    app.controller('Details', ["$scope", "$http", "$stateParams", function ($scope, $http, $stateParams) {
        function detailsFetch() {
            $http.get(dataUrlBasePath
               + "&stock_id=eq." + $stateParams.stock_id
               + "&selling_price=eq." + $stateParams.selling_price).then(function (response) {
                $scope.car = response.data[0];
                $scope.slider = [
                    $scope.car.url1.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url2.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url3.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url4.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url5.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url6.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url7.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url8.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url9.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url10.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url11.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url12.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url13.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url14.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url15.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url16.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url17.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url18.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url19.replace(/^http:\/\//i, 'https://'),
                    $scope.car.url20.replace(/^http:\/\//i, 'https://')
                ];
            });
        }

        detailsFetch(
           $stateParams.stock_id,
           $stateParams.selling_price);
        $scope.cancel = function () {
            window.history.back();
        };
    }
    ]);
})();